const getSum = (str1, str2) => {
    if (typeof str1 != 'string' || typeof str2 != 'string') {
        return false;
    }
    if (str1 == '') {
        str1 = '0';
    }
    if (str2 == '') {
        str2 = '0';
    }
    if (parseInt(str1) != str1 || parseInt(str2) != str2) {
        return false;
    }
    return (parseInt(str1) + parseInt(str2)).toString();

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let postnum = 0;
    let comnum = 0;
    listOfPosts.forEach(element => {
        if (element.author == authorName) {
            postnum++;
        }
        if (element.hasOwnProperty('comments')) {
            element.comments.forEach(comment => {
                if (comment.author == authorName) {
                    comnum++;
                }
            })
        }
    })
    return 'Post:' + postnum + ',comments:' + comnum;
};

function payment(change, vasyl) {
    for (var j = 0; j < vasyl.length; j++) {
        if (vasyl[j] > change)
            continue;
        change -= vasyl[j];


        vasyl.splice(j, 1);
        if (change != 0 && vasyl.length != 0 && vasyl[0] <= change)
            return payment(change, vasyl);
    }
    return change;
}
const tickets = (people) => {
    let vasyl = [];
    for (var i = 0; i < people.length; i++) {
        vasyl.push(people[i]);
        vasyl.sort();
        vasyl.reverse();


        let change = people[i] - 25;


        change = payment(change, vasyl);
        if (change != 0) {
            return 'NO';
        }
    }
    return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
